from decouple import config
api_token = config("API_TOKEN")

from telegram import Update
from telegram.ext import Updater, CallbackContext
from telegram.ext import CommandHandler, MessageHandler, Filters

updater = Updater(api_token)
dispatcher = updater.dispatcher

def start(update: Update, context: CallbackContext):
    chat_id = update.message.chat.id
    message_id = update.message.message_id

    context.bot.send_message(chat_id=chat_id, text="\033[31mHello, Wolrd!")


def echo(update: Update, context: CallbackContext):
    chat_id = update.message.chat.id
    message_id = update.message.message_id

    text = update.message.text

    context.bot.send_message(chat_id, text=text)


dispatcher.add_handler(CommandHandler(['start', 's', 'START'], start))
dispatcher.add_handler(MessageHandler(Filters.text & (~Filters.command), echo))
updater.start_polling()