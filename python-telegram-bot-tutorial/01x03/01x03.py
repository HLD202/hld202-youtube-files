from telegram import Update
from telegram.ext import Updater, CallbackContext
from telegram.ext import CommandHandler
from telegram import (
    InlineKeyboardMarkup,
    InlineKeyboardButton,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    KeyboardButton)
from decouple import config


API_TOKEN = config('API_TOKEN')
lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


inlineKeys = [
    [InlineKeyboardButton('1', url='google.com'), InlineKeyboardButton('2', url='google.com')],
    [InlineKeyboardButton('3', url='google.com'), InlineKeyboardButton('4', url='google.com'), InlineKeyboardButton('5', url='google.com')],
]
solidKeys = [
    [KeyboardButton('1'), KeyboardButton('2')],
    [KeyboardButton('3'), KeyboardButton('4'), KeyboardButton('5')]
]



def start(update: Update, context: CallbackContext):
    update.message.reply_text('به قسمت سوم از ساخت ربات تلگرام خوش آمدید.')


def solid_reply(update: Update, context: CallbackContext):

    solidMarkup = ReplyKeyboardMarkup(
        keyboard=solidKeys, 
        resize_keyboard=True,
        input_field_placeholder='HELLO, WORLD')

    update.message.reply_text(lorem, reply_markup=solidMarkup)


def inline_reply(update: Update, context: CallbackContext):

    inlineMarkup = InlineKeyboardMarkup(inlineKeys)
    update.message.reply_text(lorem, reply_markup=inlineMarkup)


def remove_solid_reply(update: Update, context: CallbackContext):
    update.message.reply_text('Solid Reply Removed.', reply_markup=ReplyKeyboardRemove())


def main():
    bot = Updater(API_TOKEN)
    dis = bot.dispatcher

    dis.add_handler(CommandHandler('start', start))
    dis.add_handler(CommandHandler('solid', solid_reply))
    dis.add_handler(CommandHandler('inline', inline_reply))
    dis.add_handler(CommandHandler('remove_reply', remove_solid_reply))
    bot.start_polling()


if __name__ == '__main__':
    main()



