"""
    Objectives:

        + Recive args
        + Checkout "Message" Data
        + Upload Files
        + Upload Group of File
"""

# ============================================
  ############################################
# ============================================

from decouple import config
api_token = config("API_TOKEN")

from telegram import Update
from telegram import InputMediaPhoto
from telegram.ext import Updater, CallbackContext
from telegram.ext import CommandHandler


from json import loads, dumps
def str2Json(str):
    with open('01x02/sample.json', 'w') as f:
        f.write(dumps(loads(str), indent=4))
        return 0


from os import listdir
images = {}
for img in listdir('img'):
    with open(f'img/{img}', 'br') as f:
        images[img] = f.read()


# ============================================
  ############################################
# ============================================

updater = Updater(api_token)
dispatcher = updater.dispatcher

def start(update: Update, context: CallbackContext):
    chat_id = update.message.chat.id
    message_id = update.message.message_id

    str2Json(update.message.to_json())

    context.bot.send_message(chat_id=chat_id, text="Hello, Wolrd!")

# def list_img(update: Update, context: CallbackContext):
#     chat_id = update.message.chat.id
#     message_id = update.message.message_id

#     text = "all available images\n\n"
#     for img in listdir('img/'):
#         text += f"{img}\n"

#     context.bot.send_message(chat_id, text=text, reply_to_message_id=message_id)


# def send_img(update: Update, context: CallbackContext):
#     chat_id = update.message.chat.id
#     message_id = update.message.message_id

#     args = context.args
#     for arg in args:
#         try:
#             context.bot.send_photo(chat_id, photo=images[arg], caption=arg)
#         except:
#             context.bot.send_message(
#                 chat_id, 
#                 text=f"\"{arg}\" is not in the list", 
#                 reply_to_message_id=message_id)

#             list_img(update, context)
#     return


    # context.bot.send_photo(chat_id=chat_id, photo=)


def send_all(update: Update, context: CallbackContext):
    chat_id = update.message.chat.id
    message_id = update.message.message_id

    mediaGroup = []
    for img in images:

        if img == 'image-3.jpg':
            break

        photo = InputMediaPhoto(images[img], caption=img)

        mediaGroup.append(photo)

    context.bot.send_media_group(chat_id, mediaGroup, timeout=40.0)


dispatcher.add_handler(CommandHandler(['start', 's', 'START'], start))
dispatcher.add_handler(CommandHandler('send_all', send_all))
# dispatcher.add_handler(CommandHandler('list_all', list_img))
# dispatcher.add_handler(CommandHandler('img', send_img))
updater.start_polling()